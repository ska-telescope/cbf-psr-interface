DOCKER_REGISTRY_USER_LOGIN ?=  ## registry credentials - user - set in PrivateRules.mak
CI_REGISTRY_PASS_LOGIN ?=  ## registry credentials - pass - set in PrivateRules.mak
CI_REGISTRY ?= gitlab.com/ska-telescope/cbf-psr-interface
DISPLAY := $(THIS_HOST):0

# define private overrides for above variables in here
-include PrivateRules.mak

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=src/ska:src/ska/c_vhdl_translator
PYTHON_SWITCHES_FOR_FLAKE8=--ignore=F401,W503,E203
PYTHON_SWITCHES_FOR_PYLINT=--fail-under=7

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/*.mk

